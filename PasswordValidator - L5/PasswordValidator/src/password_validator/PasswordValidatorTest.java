package password_validator;

import static org.junit.Assert.*;

import org.junit.Test;


public class PasswordValidatorTest {

	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result = PasswordValidator.hasValidCaseCharacters("A31429748as");
		assertTrue("Valid Passsword",result==true); // TODO		
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		fail("Invalid case Chars");
	}
	
	
	@Test
	public void testIsValidCaseCharsBoundaryIn() {
		boolean result=PasswordValidator.hasValidCaseCharacters("Backup");	
		assertTrue("Invalid case",result);
	}	
	
	
	@Test
	public void testIsValidCaseCharsBoundaryOutUpper() {
		boolean result=PasswordValidator.hasValidCaseCharacters("asdhhj123j");	
		assertFalse("Invalid case",result);
	}	
	
	@Test
	public void testIsValidCaseCharsBoundaryOutLower() {
		boolean result=PasswordValidator.hasValidCaseCharacters("KJBASD^%123");	
		assertFalse("Invalid Case",result);
	}
	
	
	@Test
	public void testIsValidCaseCharsExceptionNull() {
		boolean result=PasswordValidator.hasValidCaseCharacters(null);	
		assertFalse("Invalid case",result);
	}	
	
	@Test
	public void testIsValidCaseCharsExceptionSpecialChars() {
		boolean result=PasswordValidator.hasValidCaseCharacters("@$%@#%@$#$!");	
		assertFalse("Invalid Case",result);
	}
	
	@Test
	public void testIsValidCaseCharsExceptionNumbers() {
		boolean result=PasswordValidator.hasValidCaseCharacters("12385765");	
		assertFalse("Invalid Case",result);
	}
	
	
	@Test
	public final void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("12ABCDEF45");
		assertTrue("Valid Passsword",result==true); // TODO
	}
	
	
	
	@Test 
	public void testIsValidLengthException() {
		boolean result=PasswordValidator.isValidLength("");
		assertFalse("Invalid password length",result);
	}
	
	
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result=PasswordValidator.isValidLength("12ABCDEFG");	
		assertTrue("The time provided does not match the result",result);
	}
	
	
	
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result=PasswordValidator.isValidLength("12MAMIOP ");	
		assertFalse("The time provided does not match the result",result);
	}


}
